################################################################################################
#                                           Chain
################################################################################################

[Paths]
# Path for result tiled images
output : /datalocal2/share2/Asia/data_out

# Path for result Local Incidence Maps
# default: {output}/_LIA
lia : /datalocal2/share2/Asia/data_lia

# Path for result Ellipsoid Incidence Maps
# default: {output}/_IA
ia : /datalocal2/share2/Asia/data_ia

# Path to store the S1 images to be processed
s1_images : /datalocal2/share2/Asia/raw

# Path where Precise Orbit EOF files are downloaded
eof_dir : /datalocal2/share2/Asia/eof

# Special information to write in GeoTIFF metadata
# Unlike information in [Metadata], this will be written only when DEM are used
dem_info : STRM 30m

# Path to SRTM/DEM files
dem_dir : /datalocal/share/SRTM

# DEM (GPKG) database. Associates identifier keys to DEM tile geometry.
# Default for SRTM30m: ./shapefile/srtm_tiles.gpkg
# dem_database : ./shapefile/srtm_tiles.gpkg

# Pattern to find DEM files (relative to "dem_dir") from the identifier key
# extracted from the DEM database.
# Default for SRTM30m: {id}.hgt
# dem_format : {id}.hgt

# Path to geoid model
# geoid_file : ./Geoid/egm96.grd

# Path to a temporary file
tmp : /datalocal2/share2/tmp
# tmp : %(TMPDIR)s/s1tiling

[DataSource]
# Designates where the EODAG configuration file is expected to be found if not
# in %(HOME)s/.config/eodag/eodag.yml
# From S1Tiling point of view, EODAG configuration file will list the
# authentification credentials for the know providers and their respective
# priorities.
# See https://eodag.readthedocs.io/en/latest/intro.html#how-to-configure-authentication-for-available-providers
# eodag_config : %(HOME)s/.config/eodag/eodag.yml

# If True, activates the downloading from specified data provider for the ROI, otherwise only local S1 images will be processed.
download : False

# Download and unzipping are executed in parallel. Tells how many processes
# shall be used
nb_parallel_downloads : 2

# The Region of Interest (ROI) for downloading is specified in roi_by_tiles
# which will contain a list of MGRS tiles. If ALL is specified, the software
# will download all images needed for the processing (see [Processing] section)
#       ex: roi_by_tiles : 33NWB
roi_by_tiles : ALL

# Define the S1 platform that provide the products to process.
# Must be: empty, or a comma-separated list of platform names like "S1A,
# S1C"...
platform_list :

# Define the polarisation mode of the products to downloads
# Must be "HH-HV", "VV-VH", "VV", "VH", "HV", or "HH"
polarisation : VV-VH


# Download only the products acquired in ASCending or in DEScending order.
# By default (when left unspecified), no filter is applied.
# Warnings:
# - this filter only applies to download, and not to products already
#   downloaded.
# - each relative orbit is exclusive to one orbit direction, orbit_direction
#   and relative_orbit_list shall be considered as exclusive
# orbit_direction : DES

# Download only the products from the specified relative orbits.
# By default (when left unspecified), no filter is applied.
# Warnings:
# - this filter only applies to download, and not to products already
#   downloaded.
# - each relative orbit is exclusive to one orbit direction, orbit_direction
#   and relative_orbit_list shall be considered as exclusive
# relative_orbit_list = 80, 161

# Percentage of tile area to be covered for a pair of Sentinel-1 products to be
# retained.
# tile_to_product_overlap_ratio : 50

# Please, set the initial and final date (YYYY-MM-DD format) in this line
# Don't touch the other parameters
first_date : 2018-08-01
last_date : 2019-07-31

[Mask]
# This option allows you to choose if you want to generate border mask.
generate_border_mask: False

[Processing]
# Tells whether DEM files are copied in a temporary directory, or if symbolic
# links are to be created.
# For performance reasons with OTB 7.X, it's better to regroup the minimal
# subset of the DEM files required for processing. Symbolic links work fine
# most of the time, however if the files are on a remote shared filesystem
# (GPFS, NAS...), performances will be degraded. In those cases, it's better to
# copy the required DEM files on a local filesystem.
# Two values are supported for this option: "copy" and "symlink" (default).
cache_dem_by: symlink

# Define the type of calibration: gamma, or sigma or normlim
calibration: sigma

# Remove thermal noise
remove_thermal_noise: True

# Noise removal may set some pixel values to 0.
# However, 0, is currently reserved by S1Tiling chain as a "nodata" value
# introduced by Margin Cutting and Orthorectification.
# The following parameter defines which value to use instead of 0 when noise
# is removed.
# By default: 1e-7 will be used.
lower_signal_value : 1e-7

# Pixel Size (in meters) of the output images
output_spatial_resolution : 10.

# Path and filename of the tile shape definition (ESRI Shapefile)
# tiles_shapefile : ./shapefile/Features.shp

# Grid spacing for the interpolator in the orthorectification process
# for more information, please consult the OTB orthorectification application
# A nice value is 4x output_spatial_resolution
orthorectification_gridspacing : 40

# Interpolation method used in the orthorectification process
# for more information, please consult the OTB orthorectification application
# Default value is set to nearest neighbor interpolation (nn) to keep compatibilty with previous results
# By the way linear method could be more interesting.
# Note that the bco method is not currently supported
orthorectification_interpolation_method : nn

# Tiles to be processed
# The tiles can be given as a list
# - tiles: list of tiles (comma separated)
#     ex: tiles: 33NWB,33NWC
# - tiles_list_in_file: tile list in a ASCII file
#     ex: tiles_list_in_file : ~/MyListOfTiles.txt
tiles : 33NWB

# Running mode:
# Normal: print normal, warning and errors on screen
# debug: also print debug messages
# logging: save logs to files
mode : debug logging

# Number of processes to be running in parallel
# This number defines the number of S1 images to be processed in parallel.
# Must be <= to the number of cores on the machine
nb_parallel_processes : 2

# RAM Allower per process in MB
ram_per_process : 4096

# Numbers of threads used by each OTB application
# For an optimal performance, nb_parallel_processes*nb_otb_threads should be <= to the number of core on the machine
nb_otb_threads: 4

# Tell to generate the LIA map in degrees (x100) when the LAI sine map is
# generated.
# This option is ignored when no LIA sine map is produced.
# (The LIA sine map is produced by S1LIAMap program, or when calibration mode
# is "normlim")
produce_lia_map : False

# List to select which IA maps will be produced by S1IAMap program.
# By default, the maps of incidence angles values in degrees x 100 is produced
# Possible values are: deg, cos, sin, tan
ia_maps_to_produce : deg, cos

# Resampling method used by gdalwarp to project DEM on S2 tiles for LIA computation purposes
# Valid choices are: near, bilinear, cubic, cubicspline, lanczos, average, rms,
# mode, max, min, med, q1, q3, qum
# dem_warp_resampling_method = cubic


# Options to control the names of the result files.
# Advice: don't change the names of the intermediary products. Act only on the
# final products.
#
# * Intermediary products
# fname_fmt.calibration            : {rootname}_{calibration_type}_calOk.tiff
# fname_fmt.cut_borders            : {rootname}_{calibration_type}_OrthoReady.tiff
# fname_fmt.correct_denoising      : {rootname}_{calibration_type}_NoiseFixed.tiff
# fname_fmt.orthorectification     : {flying_unit_code}_{tile_name}_{polarisation}_{orbit_direction}_{orbit}_{acquisition_time}_{calibration_type}.tif
# fname_fmt.dem_s2_agglomeration   : DEM_{tile_name}.vrt
# fname_fmt.dem_on_s2              : DEM_projected_on_{tile_name}.tiff
# fname_fmt.height_on_s2           : DEM+GEOID_projected_on_{tile_name}.tiff
# fname_fmt.ground_and_sat_s2      : XYZ_projected_on_{tile_name}_{orbit_direction}_{orbit}.tiff
#
# * Deprecated products
# fname_fmt.dem_s1_agglomeration   : DEM_{polarless_rootname}.vrt
# fname_fmt.s1_on_dem              : S1_on_DEM_{polarless_basename}
# fname_fmt.xyz                    : XYZ_{polarless_basename}
# fname_fmt.lia_orthorectification : {LIA_kind}_{flying_unit_code}_{tile_name}_{orbit_direction}_{orbit}_{acquisition_time}.tif
# fname_fmt.lia_concatenation      : {LIA_kind}_{flying_unit_code}_{tile_name}_{orbit_direction}_{orbit}_{acquisition_day}.tif
# fname_fmt.s1_lia                 : LIA_{polarless_basename}
# fname_fmt.s1_sin_lia             : sin_LIA_{polarless_basename}
#
# * Final products
# - Output directories
# dname_fmt.tiled                  : '{out_dir}/{tile_name}'
# dname_fmt.mask                   : '{out_dir}/{tile_name}'
# dname_fmt.filtered               : '{out_dir}/filtered/{tile_name}'
# dname_fmt.lia_product            : '{lia_dir}'
#
# TODO: add masks
# - Concatenation value for all calibrations but normlim
# fname_fmt.concatenation          : {flying_unit_code}_{tile_name}_{polarisation}_{orbit_direction}_{orbit}_{acquisition_stamp}.tif
# fname_fmt.filtered               : {flying_unit_code}_{tile_name}_{polarisation}_{orbit_direction}_{orbit}_{acquisition_stamp}_filtered.tif
#
# - Concatenation value for normlim calibration
# fname_fmt.lia_product            : {LIA_kind}_{flying_unit_code}_{tile_name}_{orbit_direction}_{orbit}.tif
# fname_fmt.concatenation          : {flying_unit_code}_{tile_name}_{polarisation}_{orbit_direction}_{orbit}_{acquisition_stamp}_{calibration_type}.tif
# fname_fmt.s2_lia_corrected       : {flying_unit_code}_{tile_name}_{polarisation}_{orbit_direction}_{orbit}_{acquisition_stamp}_NormLim.tif
# fname_fmt.filtered               : {flying_unit_code}_{tile_name}_{polarisation}_{orbit_direction}_{orbit}_{acquisition_stamp}_{calibration_type}_filtered.tif
#
#

[Filtering]
# Speckle filtering method: none/Lee/Frost/Gammamap/Kuan
filter :

# Set the window radius for the spatial filtering
# Take care that it is a radius, i.e. radius=1 means the filter does an 3x3 pixels averaging.
window_radius : 2

# Deramp factor, for Frost filter only
deramp : 0.1

# Number of looks, for all but Frost => Lee, Gammamap and Kuan
nblooks: 1.0

# If not caring for non filtered product (and if filter method is specified),
# then the orthorectified and concatenated products won't be considered as
# mandatory and they will not be kept at the end of the processing.
# This (exclusion) feature cannot be used alongside generate_border_mask
# (i.e. keep_non_filtered_products cannot be False if generate_border_mask
# is True)
# Note: It's only supported after LIA calibration as of V1.0 of S1Tiling.
# See Issue #118.
keep_non_filtered_products : True

# # If True, the outcore of the multiImage filter is reset before filtering. It means that the outcore is recomputed from scratch with the new images only.
# # If False, the outcore is updated with the new images. Then, the outcore integrates previous images and new images.
# reset_outcore : True

[Metadata]
contact : Your Name <your.email@your.wo.rk>
any thing : you want added in the output image metadata
